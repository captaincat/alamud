# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .action import Action2
from mud.events import DrinkEvent

class DrinkAction(Action2):
    EVENT = DrinkEvent
    ACTION = "drink"
    RESOLVE_OBJECT = "resolve_for_use"
