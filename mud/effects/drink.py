# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .effect import Effect2
from mud.events import DrinkEvent

class DrinkEffect(Effect2):
    EVENT = DrinkEvent