# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .effect import Effect2
from mud.events import SleepEvent

class SleepEffect(Effect2):
    EVENT = SleepEvent
