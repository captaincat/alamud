# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .effect import Effect2
from mud.events import SpeakEvent

class SpeakEffect(Effect2):
    EVENT = SpeakEvent
