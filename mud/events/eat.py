# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .event import Event2

class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("edible"):
            self.fail()
            return self.inform("eat.failed")
        self.inform("eat")
