# -*- coding: utf-8 -*-
# Copyright (C) 2016 Pierre Chat & Nam Ly
#==============================================================================

from .event import Event2

class SpeakEvent(Event2):
    NAME = "speak"

    def perform(self):
        if not self.object.has_prop("speakable"):
            self.fail()
            return self.inform("speak.failed")
        self.inform("speak")
